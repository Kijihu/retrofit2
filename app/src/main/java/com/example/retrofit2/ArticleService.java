package com.example.retrofit2;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ArticleService {

    @GET("v1/api/articles")
    Call<ArticleRespone> findAll(@Query("page") long page,
                                          @Query("limit") long limit);

}
