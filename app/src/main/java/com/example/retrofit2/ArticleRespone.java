package com.example.retrofit2;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.http.GET;

public class ArticleRespone {

    @SerializedName("CODE")
    private String code;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("DATA")
    private List<ArticleEntity> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ArticleEntity> getData() {
        return data;
    }

    public void setData(List<ArticleEntity> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ArticleRespone{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
