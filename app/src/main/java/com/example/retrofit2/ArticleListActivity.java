package com.example.retrofit2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

public class ArticleListActivity extends AppCompatActivity {

    private ArticleRespone dataSet;
    private RecyclerView rvArticle;
    private ArticleListAdapter adapter;
    private ArticleRepository articleRespository;
    private ProgressBar pbLoading;
    private SwipeRefreshLayout pullRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_list);

        rvArticle = findViewById(R.id.rv_article);

        pbLoading = findViewById(R.id.pb_loading);

        pullRefresh = findViewById(R.id.pull_refresh);

        pullRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchData();
            }
        });

        articleRespository = new ArticleRepository();
        fetchData();
    }

    private void setupRecyclerView() {
        adapter = new ArticleListAdapter(this, dataSet);
        rvArticle.setAdapter(adapter);
        rvArticle.setLayoutManager(new LinearLayoutManager(this));
        adapter.notifyDataSetChanged();
    }

    private void fetchData() {
        dataSet = articleRespository.findAllArticles(1, 10, new onSuccessCallBack() {
            @Override
            public void onSuccess() {
                setupRecyclerView();
                pbLoading.setVisibility(View.GONE);
                pullRefresh.setRefreshing(false);
            }
        });
    }
}
