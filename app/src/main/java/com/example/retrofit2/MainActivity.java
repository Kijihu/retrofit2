package com.example.retrofit2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArticleService articleService =
                ServiceGenerator.createService(ArticleService.class);

        Call<ArticleRespone> callArticle = articleService.findAll(1, 5);

        callArticle.enqueue(new Callback<ArticleRespone>() {
            @Override
            public void onResponse(Call<ArticleRespone> call, Response<ArticleRespone> response) {
                if (response.isSuccessful() && response.body() != null){
                    ArticleRespone articleRespone = response.body();
                    Log.d(TAG, "onResponse: " + articleRespone.getData().get(0));
                }
            }

            @Override
            public void onFailure(Call<ArticleRespone> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
    }
}
