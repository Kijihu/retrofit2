package com.example.retrofit2;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {

    private final static String TAG = "Article";

    private ArticleService articleService;

    public ArticleRepository() {
        articleService = ServiceGenerator.createService(ArticleService.class);
    }

    public ArticleRespone findAllArticles(int page, int limit, final onSuccessCallBack callback) {
        final ArticleRespone dataSet = new ArticleRespone();
        articleService.findAll(page, limit).enqueue(new Callback<ArticleRespone>() {
            @Override
            public void onResponse(Call<ArticleRespone> call, Response<ArticleRespone> response) {
                if (response.isSuccessful()) {
                    Log.e(TAG, "onResponse: Successfully");
                    dataSet.setCode(response.body().getCode());
                    dataSet.setMessage(response.body().getMessage());
                    dataSet.setData(response.body().getData());
                    callback.onSuccess();
                }
            }

            @Override
            public void onFailure(Call<ArticleRespone> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
        return dataSet;
    }

}
